<?php

class intellectmoney_invoice extends nc_payment_invoice {

    public function __construct($invoice) {
        $this->properties = $invoice->properties;
    }

    public function get_amount($format = NULL) {
        return number_format($this->get('amount'), 2, '.', '');
    }

    public function get_id() {
        return $this->get('id');
    }

    public function get_order_id() {
        return $this->get('order_id');
    }

    public function get_customer_email() {
        return $this->get('customer_email');
    }

    public function get_customer_name() {
        return $this->get('customer_name');
    }

}

class nc_payment_system_intellectmoney extends nc_payment_system {

    const TARGET_URL = "https://merchant.intellectmoney.ru/ru/";

    protected $automatic = TRUE;
    protected $accepted_currencies = array('RUB', 'TST', 'RUR');
    protected $settings = array(
        'IntellectMoney EshopId' => null,
        'IntellectMoney SecretKey' => null,
        'IntellectMoney IsTest' => null,
        'IntellectMoney HoldMode' => null,
        'IntellectMoney ExpireDate' => null,
        'IntellectMoney Preferences' => null,
        'IntellectMoney StatusCreate' => null,
        'IntellectMoney StatusPay' => null,
        'IntellectMoney StatusHold' => null,
        'IntellectMoney StatusPartiallyPay' => null,
        'IntellectMoney StatusCansel' => null,
        'IntellectMoney SuccessUrl' => null,
        'IntellectMoney FailUrl' => null,
    );
    protected $request_parameters = array(
    );
    protected $callback_response = array(
        'eshopId' => null,
        'paymentId' => null,
        'orderId' => null,
        'eshopAccount' => null,
        'serviceName' => null,
        'recipientAmount' => null,
        'recipientCurrency' => null,
        'paymentStatus' => null,
        'userName' => null,
        'userEmail' => null,
        'paymentData' => null,
        'payMethod' => null,
        'secretKey' => null,
        'hash' => null,
        'UserField_1' => null,
    );

    private static function ob_exit($status = null) {
        if ($status) {
            isset($_REQUEST['debug']) ? exit($status) : exit($status);
        } else {
            header("HTTP/1.0 200 OK");
            echo "OK";
            exit();
        }
    }

    public function execute_payment_request(nc_payment_invoice $invoice) {
        $im_invoice = new intellectmoney_invoice($invoice);
        $current_currency = array('RUR', 'RUB');

        $eshopId = $this->get_setting('IntellectMoney EshopId');
        $secretKey = $this->get_setting('IntellectMoney SecretKey');
        $orderId = $im_invoice->get_order_id();
        $invoiceId = $im_invoice->get_id();

        $recipientAmount = $im_invoice->get_amount();
        $serviceName = $im_invoice->get_description();
        if ($this->is_active_field('IntellectMoney IsTest')) {
            $currency = 'TST';
        } else {
            $currency = in_array(strtoupper($im_invoice->get_currency()), $current_currency) ? 'RUB' : 'TST';
        }

        $holdMode = '0';
        if ($this->is_active_field('IntellectMoney HoldMode')) {
            $holdMode = '1';
            $day = $this->get_setting('IntellectMoney ExpireDate');
            if (intval($day) && $day > 0 && $day < 6) {
                $expireDate = date('Y-m-d 00:00:00', strtotime('+' . $day . ' day'));
            } else {
                $expireDate = date('Y-m-d 00:00:00', strtotime('+3 day'));
            }
        }

        $userEmail = $im_invoice->get_customer_email();
        $userName = $im_invoice->get_customer_name();
        $preference = $this->get_setting("IntellectMoney Preferences");
        $successUrl = $this->get_setting('IntellectMoney SuccessUrl');
        $failUrl = $this->get_setting('IntellectMoney FailUrl');

        $hashArray = array($eshopId, $orderId, $serviceName, $recipientAmount, $currency, $secretKey);
        $hash = md5(join('::', $hashArray));

        $form = "
            <html>
              <body>
                    <form method='POST' action='https://merchant.intellectmoney.ru/ru/'>" .
                $this->make_inputs(array(
                    'eshopId' => $eshopId,
                    'orderId' => $orderId,
                    'recipientAmount' => $recipientAmount,
                    'serviceName' => $serviceName,
                    'recipientCurrency' => $currency,
                    'holdMode' => $holdMode,
                    'expireDate' => isset($expireDate) ? $expireDate : '',
                    'userName' => $userName,
                    'preference' => $preference,
                    'userEmail' => $userEmail,
                    'successUrl' => $successUrl,
                    'failUrl' => $failUrl,
                    'UserField_1' => $invoiceId,
                    'hash' => $hash,
                )) . "
                </form>
                <script>
                  document.forms[0].submit();
                </script>
              </body>
            </html>
            ";
        echo $form;
        exit;
    }

    private function is_active_field($field_name) {
        return strtoupper($this->get_setting($field_name)) === 'Y' || $this->get_setting($field_name) === '1';
    }

    public function on_response(nc_payment_invoice $invoice = null) {
        
    }

    public function validate_payment_request_parameters() {
//        if (!preg_match("/^4\d{5}$/", $this->get_setting('eshopId'))) {
//            $this->add_error(nc_payment_system_webmoney::ERROR_PURSE_IS_NOT_VALID);
//        }
//        if (!preg_match("/^.*$/", $this->get_setting('secretKey'))) {
//            $this->add_error(nc_payment_system_webmoney::ERROR_PURSE_IS_NOT_VALID);
//        }
    }

    public function validate_payment_callback_response(nc_payment_invoice $invoice = null) {
        $im_invoice = new intellectmoney_invoice($invoice);

        $innerSecretKey = $this->get_setting('IntellectMoney SecretKey');
        $innerEshopId = $this->get_setting('IntellectMoney EshopId');

        $orderAmount = $im_invoice->get_amount();
        $orderCurrency = $this->is_active_field('IntellectMoney IsTest') ? "TST" : $im_invoice->get_currency();

        $eshopId = $this->get_response_value('eshopId');
        $paymentId = $this->get_response_value('paymentId');
        $orderId = $this->get_response_value('orderId');
        $eshopAccount = $this->get_response_value('eshopAccount');
        $serviceName = $this->get_response_value('serviceName');
        $recipientAmount = $this->get_response_value('recipientAmount');
        $recipientCurrency = $this->get_response_value('recipientCurrency');
        $paymentStatus = $this->get_response_value('paymentStatus');
        $userName = $this->get_response_value('userName');
        $userEmail = $this->get_response_value('userEmail');
        $paymentData = $this->get_response_value('paymentData');
        $payMethod = $this->get_response_value('payMethod');
        $secretKey = $this->get_response_value('secretKey');
        $hash = $this->get_response_value('hash');
        $invoiceId = $this->get_response_value('UserField_1');

        $allPaymentStatus = array(3, 4, 5, 6, 7);
        $statuses = array(
            '3' => 'IntellectMoney StatusCreate',
            '4' => 'IntellectMoney StatusCansel',
            '5' => 'IntellectMoney StatusPay',
            '6' => 'IntellectMoney StatusHold',
            '7' => 'IntellectMoney StatusPartiallyPay'
        );

        $control_hash_str = implode('::', array(
            $innerEshopId, $orderId, $serviceName,
            $eshopAccount, $recipientAmount, $orderCurrency, $paymentStatus,
            $userName, $userEmail, $paymentData, $innerSecretKey
        ));

        $control_hash_str = urldecode($control_hash_str);
        $control_hash = md5($control_hash_str);
        $control_hash_utf8 = md5(iconv('windows-1251', 'utf-8', $control_hash_str));


        if ($orderAmount !== $recipientAmount) {
            nc_payment_system_intellectmoney::ob_exit('RECIPIENTAMOUNT MISMATCH Control recipientAmount:' . $orderAmount . ' Post recipientAmount:' . $recipientAmount);
        }

        if ($orderCurrency !== $recipientCurrency) {
            nc_payment_system_intellectmoney::ob_exit('RECIPIENTCURRENCY MISMATCH Control recipientCurrency:' . $orderCurrency . 'Post recipientCurrency:' . $recipientCurrency);
        }

        if ($eshopId !== $innerEshopId) {
            nc_payment_system_intellectmoney::ob_exit('ESHOPID MISMATCH Control eshopId:' . $innerEshopId . ' Post eshopId:' . $eshopId);
        }

        if (($hash != $control_hash && $hash != $control_hash_utf8) || !$hash) {
            $err = "ERROR: HASH MISMATCH\n";
            $err .= "Control hash string: $control_hash_str;\n";
            $err .= "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $hash;\n\n";
            $this->add_error($err);
        }

        if (in_array($paymentStatus, $allPaymentStatus)) {
            $statusNumber = $this->get_setting($statuses[$paymentStatus]);
            
            $this->on_payment_success($invoice);
            $invoice->set('status', $statusNumber);
            $invoice->save();
            
            $netshop = nc_netshop::get_instance();
            $order = $netshop->load_order($orderId);
            $order->set('Status', $statusNumber);
            $order->save();
            nc_payment_system_intellectmoney::ob_exit();
        } else {
            nc_payment_system_intellectmoney::ob_exit('paymentStatus Not Found');
        }
    }

    public function load_invoice_on_callback() {
        return $this->load_invoice($this->get_response_value('UserField_1'));
    }

}
