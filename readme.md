#Модуль оплаты платежной системы IntellectMoney для CMS NetCat

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/NetCat#whatnew.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/NetCat#557714de663370afe342a885a0a36a2619c3a7
